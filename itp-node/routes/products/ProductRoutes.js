const express = require("express"),
    router = express.Router(),
    ProductController = require("./ProductController");

router.post("/", ProductController.getAllList);

module.exports = router;