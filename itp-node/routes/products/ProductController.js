const Product = require("../../models/Product");

class ProductController {
    async getAllList(req, res) {
        // console.log("body", req.body);
        let { page, limit, keywords } = req.body;

        limit = (limit != undefined && limit > 0) ? parseInt(limit) : parseInt(process.env.pageLimit);

        page = parseInt(page);

        let offset = limit * page;
        let query = {};

        if (keywords != undefined && keywords != '') {
            query = { primaryName: new RegExp(keywords, "i"), ...query }
        }
        try {
            let result = await Product.aggregate([
                {
                    $match: query
                },
                // {
                //     $count: "totalProducts"
                // },
                { $skip: offset },
                { $limit: limit }
            ]);
            // console.log("data", result);
            return res.status(200).json({ status: true, data: result });
        } catch (error) {
            console.log("error", error);
            return res.status(200).json({ status: false, data: [], msg: "Error" });
        }
    }
}


module.exports = new ProductController();