const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const ProductSchema = new Schema({
    nconst: {
        type: String,
    },
    primaryName: {
        type: String,
        required: true,
        trim: true
    },
    birthYear: {
        type: Number
    },
    deathYear: {
        type: Number
    },
    primaryProfession: {
        type: String,
        trim: true
    },
    knownForTitles: {
        type: String,
        trim: true
    }
});

ProductSchema.index({"nconst": 1, "primaryName": 1});

module.exports = mongoose.model("Product", ProductSchema);