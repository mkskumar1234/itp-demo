import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private API_ENDPOINT = environment.apiUrl;

  constructor(private http: HttpClient) {

  }
  // get all product listing
  getList(payload): Observable<any[]> {
    let headersOptions: HttpHeaders = new HttpHeaders();
    headersOptions = headersOptions.append('Accept', 'application/json');
    headersOptions = headersOptions.append('Content-Type', 'application/json')
    headersOptions = headersOptions.append('id', "1")
    return this.http.post<any[]>(this.API_ENDPOINT + 'api/v1/products', payload, { headers: headersOptions })
  }
}
