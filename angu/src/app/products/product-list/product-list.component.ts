import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductsService) { }
  page: number = 0;
  limit: number = 30;
  productList = [];

  filterParams: any = {
    page: this.page,
    limit: this.limit,
    keywords: 'John Belu'
  }

  ngOnInit(): void {
    this.getProductList();
  }

  getProductList(){
    console.log("--getProductList-");

    this.productService.getList(this.filterParams).subscribe((result: any)=>{
      console.log("--result",result);
      if(result.status){
        this.productList = result.data;
      }else{
        console.log("--test");
      }
    })
  }

  pageChange(event:any){
    console.log("-event",event)
  }
  

}
