import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { ProductListRoutingModule } from './product-list-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductListRoutingModule,
    InfiniteScrollModule
  ]
})
export class ProductListModule { }
