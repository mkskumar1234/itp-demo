import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';

const routes: Routes = [{
    path: '',
    component: ProductListComponent,
    children: [
      { path: 'list', loadChildren: () => import('./product-list/product-list.module').then(m => m.ProductListModule) },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
